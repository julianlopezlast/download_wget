# download_wget

Para descargar el script utilizando `wget` ir al archivo desde el navegador web y copiar el enlace del botón _"Permalink"_.

Luego:
```
> wget https://gitlab.com/gfpp/download_wget/-/blob/fb78238307d126a2cea394c43ca019ca5a67d907/script/dump_odom.py
```
